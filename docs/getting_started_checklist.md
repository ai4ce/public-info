# Welcome to AI4CE. So great to have you!

Now its time to get you started

1. Please, to move foreward, provide me with details (username) to the following accounts:
  - TU Darmstadt: Your full name and Matrickelnummer, stud email address
  - GiLlab https://gitlab.com  for open source code organisation
  - Element https://element.io/  for open source and secure communication
2. Join the Element Welcome room: [Element](https://matrix.to/#/#ai4ce:matrix.org)
3. Install [LibreOffice](https://www.libreoffice.org/download/download-libreoffice/)
  - **NO** Microsoft Powerpoint for the weekly presentations
  - **NO** Google Presentations for the weekly presentations

We recommend using our prepared DevBox - you will get the link to it via mail.
Requirements: 8+GB system ram, 60GB available disk space
For that you need to download and install VirtualBox + ExtensionPack [virtualbox.org](https://www.virtualbox.org/wiki/Downloads)

# Weekly Meetings
- mandatory: Wednesday 12:30-15:00 - L101 368 (CEL)
- **NO LAPTOP WORK DURING THE MEETING** - don't be rude to the presentations of others

# What is the DevBox?
To ensure that all participating students have a good starting point to run the software they need, AI4CE provides you with a virtual machine (VM) Linux image, based on Linux Lite.
Its a fully prepared Linux VM with all necessary software already installed. All you have to do is install the VM software, as indicated in the getting started guide and connect your GitLab ssh key with the prepared Linux VM.

Besides supporting smooth development, this VM also helps you in another way: By everyone using the same development environment, we ensure consistency in the tool we provide each other and we can offer support for the same problems.
Thats why we can not offer technical support for people not using the DevBox.

# Presentation Training
- explain the title
- give context of your study
- only show, what you talk about
- ~90sec per slide is a good rule of thump
- if you show an agenda: make at least 5 min blocks: 20min -> 4 sections in the agenda
- between section: give context by summarising what discussed before (~7sec)
- show graphs (we are engineers. we love data)
- ALWAYS EXPLAIN **EVERY** GRAPH/PLOT -> axis, analysis, outliers
- focus on Requirements, how to validate your implementation against the reqs and what the validation teaches you
- highlight the assumptions/short cuts which form the design boundaries of your work
- work with the template. You can adapt things. Its a template, not a rule of nature.
- wear something appropiate
- be calm and precise
- never cross your arms
- look at the audience, not the slides
- end your presentation on a summary slide
- you are not your work. You make a study/reserach project and you present what you did and what your best analysis of this is. It is encouraged to be critical with your own approach, if you find aspects to improve during your work -> outlook
- make implizit things, explizit
- "not enough time" is **NEVER** a good explaination. You focused on the essentials. 
- no text < text < tables < graphic < table & highlights < graphic & highlights 
- the final presentation is moment of celebration. You can be proud of what you have achieved
- 