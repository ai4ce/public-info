# Public Info

![Overview of the AI4CE Concept](https://gitlab.com/ai4ce/public-info/-/raw/main/docs/graphics/ai4ce_platform_overview_20231031.png)\
The AI4CE platform will assist the CE process in its early design phases by providing an environment to design, test, validate and integrate system generation methods.

You can find the latest intro slides here: [AI4CE Intro v4 - GitLab](https://gitlab.com/ai4ce/public-info/-/blob/main/docs/AI4CE_intro_v4.pdf)

# What is AI4CE?

[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/HvlRNJvW8to/0.jpg)](https://youtu.be/HvlRNJvW8to)


Artificial Intelligence for Concurrent Engineering (AI4CE) is an open source research project at the Technical University Darmstadt together with Parametry.ai, with which AI-based system generation methods for the application in space system engineering's Phase 0/A studies can be formalised, implemented and verified.
AI4CE combines the data-centric advantages of modern Artificial Intelligence (AI) and Machine Learning (ML) methods with the proven engineering effectiveness of Concurrent Engineering (CE) to further enhance the system development process.

Previous research for AI-based system creation introduced multiple philosophies to support the CE process - extracting knowledge from historical mission data (top down) vs. generating design knowledge from equations and experience (bottom up). While these engineering approaches offer advantages and disadvantages, a scheme to compare or even combine these approaches is still to be discovered.
Therefore, AI4CE will provide a digital platform, to not only generate space systems based on numerous AI-methods and but also to test and compare these generations. 

The project focusses mainly on the application for space systems (CubeSats, Small Sats, ...) but the idea is to provide a general CE-support platform to implement and test system generation methods.
The vision is to be able to generate a system (in form of a component list from a database) for any kind of system, with just the underlying mathematical model for the system architecture and the component database.


The platform is currently envisioned to contain the following aspects:
- a **GUI builder**, where the user can define the bewished system architecture
- a module to **implement** various AI- and non-AI-based system generation methods
- focus on **bottom-up** system creation
- a comparison/**benchmarking** module to benchmark the system creation outcome of the various creation methods
- **interfaces** to connect and interact with other software in the MBSE ecosystem
- improved system creation democracy and trust as well as business-logic automation achieved through decentralisation (a private blockchain)

![AI4CE Poster for AIstart 2023](https://gitlab.com/ai4ce/public-info/-/raw/main/conferences/ai4ce_poster_v03_whitebg.png)



Further Links:
- Project GitLab https://gitlab.com/ai4ce/public-info
- My Standard Entry point https://jan-peter.space/ai4ce/


Delta to original project idea (bottom-up):
- a single system builder is now a system builder comparison platform
- much better approach on system creation validation
- incorporation of top-down and bottom-up system creation ideas

![Task Workflow](https://gitlab.com/ai4ce/public-info/-/raw/main/docs/graphics/ai4ce_tasks-workflow.png)

# Open Bachelor/Master/ADP projects
- 🔍 **Implementation of a "find n-other" System Generation** \
*Keywords:* AI infrastructure, AI, Reinforcement Learning\
*JP's Comment:* <span style='color: #ff00ff;'>***HIGHLY NEEDED***</span>
- 🕹️ **Implementation of a RL env switcher** \
*Keywords:* AI infrastructure\
*JP's Comment:* <span style='color: #ff00ff;'>***HIGHLY NEEDED***</span>
- &#x269B; **Implementation of a Quantum Solver** <u>in cooperation with ESA/ESOC</u>\
*Keywords:* Quantum Computing, Dashboard Development\
*JP's Comment:* <span style='color: #ff00ff;'>***HIGHLY NEEDED***</span>
- ⚖️ **Implementation of static optimisation algorithms and comparison against AI-based System Generator** <u>in cooperation with DLR and GSI</u>\
*Keywords:* Optimisation --> [CERN's geoff framework](https://gitlab.cern.ch/geoff/geoff-app)\
*JP's Comment:* <span style='color: #ff00ff;'>***HIGHLY NEEDED***</span>
- 🔀 **System Modelling using SysMLv2** <u>in cooperation with a special partner</u>\
_Keywords_: system modelling, MBSE\
*JP's Comment:* <span style='color: #ff00ff;'>***HIGHLY NEEDED***</span>
---
- [ ] RL from Human Feeback for reward function estimation / Inverse RL
- [ ] Towards Explainability in AI-based Satellite Design Generation 
- [ ] Development of a AR/VR System Viewer <u>with industry and DLR support</u>\
_Keywords_: 3D modelling, AR/VR \
*JP's Comment:* [3D printable Smartphone Headset](https://www.thingiverse.com/thing:729029)
- [ ] Development of a Digital Twin concept for automated system generation 
_Keywords_: digital twin
- [X] ADP/MT **Development of a CubeSat design validation with simulation**\
_Keywords_: CubeSat, system design, AI
  - using: [Ansys STK](https://www.ansys.com/products/missions/ansys-stk), [NASA's GMAT](https://software.nasa.gov/software/GSC-18094-1), [NASA's MDAO](https://software.nasa.gov/software/LEW-18550-1). [CubeSim](https://github.com/bernhard-seifert/CubeSim)
- [X] **Dealing with system requirements using tools like ReqIF**\
*Keywords:* Requirement Engineering, MBSE
- [ ] Clouds for modelling system design uncertainty https://www.esa.int/gsp/ACT/projects/clouds/
---
- [ ] BT/ADP/MT: **Definition of acceptence criteria for an AI-based design creator**\
_Keywords_: AI, system model, HMI, psychologie
- [ ] ADP **Research and Comparison of generative system generation methods across multiple industries**\
*Keywords*: research, system engineering, digital engineering, future engineering, Industry 4.0, digitalisation
- [X] BT/ADP/MT: **Development of a Satellite Design Evaluation Benchmark**\
_Keywords_: CubeSat, system design, HMI, creative engineering
- [ ] ADP/MT: **Development of a Concept to explain the AI-based system generator's decision**\
_Keywords_: explainable AI, system model, HMI, psychologie
- [ ] ADP/MT **Transfer Learning of AI models between new system requirements**\
_Keywords_: CubeSat, system design, AI, Transfer Learning, Machine Learning, Reinforcement Learning
- [ ] ADP/MT **Import/Export of generated satellite system into Valispace**\
_Keywords_: MBSE, company interaction
- [X] ADP/MT **Import/Export of generated satellite system into COMET**\
_Keywords_: MBSE, company interaction
- [X] ADP/MT **Import/Export of generated satellite system into Virtual Satellite 4**\
_Keywords_: MBSE, company interaction
- [ ] ADP/MT **Import/Export of generated satellite system into Dessia**\
_Keywords_: MBSE, company interaction
- [X] ADP/MT **Development of an system diagram generator for an interactive system diagram**\
_Keywords_: SysML v2, System Engineering
- [ ] ADP/MT **Understanding of Design Quality through Music**\
_Keywords_: System Engineering, Music, Accustic validation, (generative AI) - Starting Point: [Strudel Music Generator](https://strudel.cc/)
- [ ] MT **Development of a Quantum-Solver for a CubeSat system generator**\
_Keywords_: System Engineering, AI, Quatum Computing, ESOC, CubeSat design
- [ ] MT/ADP **Introspect usage of COMPASS for the AI4CE system concept generator**\
_Keywords_: Model verification, satellite design

Contact ceglarek@fsr.tu-darmstadt.de for any more information

# Previous Bachelor/Master/ADP/ARP projects
- [X] Development of a generative 3D CubeSat\
Keywords: CAD, 3D printing, CubeSat
- [X] Research and Comparison of MBSE usages across multiple industries\
Keywords: MBSE, digital engineering, research
- [X] Design of a survey for requirements on AI supported system design assistance\
Keywords: concurrent engineering, DRL, ESA, future engineering
- [X] Development of an adaptive component generator and database\
Keywords: programming, space system engineering, cubesat
- [X] Development of a GUI for the AI4CE platform\
Keywords: HMI, GUI, web technology, front end
- [X] Implementation and Comparison of AI/RL algorithms\
Keywords: AI, Reinforcement Learning, Deep Learning

# Presentations/Papers
- IAC2022
- MBSE2022
- SECESA2022
- SpaceOps2023
- ESOC Innovation Café Vol.5

## Addional:
- Explainable AI
- Online Learning
- Large language models
- Natural Language Processing
- Knowledge Graphs
- Blockchain
- other AI algorithms and methods

# Techstack
- Python
- Streamlit (Python Web-Frontend)
- Docker
- Linux

# How can AI4CE support the CE process?
AI4CE provides a software tool, where the CE experts can generate system solutions according to their needs.
This tool functions as a platform, where multiple approaches for the system generation can be compared to each other. 

# What is Concurrent Engineering?
Concurrent Engineering (CE) is a engineering design process, in which all stakeholders of the to-be-developed system work concurrently on the system's design.
Organised in a CE study, all partners sit together in one dedicated facility regulary over the span of a 2 to 6 weeks (traditionally) and diskuss first ideas of how the requirements of a technical system can be met with technical solutions.


---
This repo is about everything that is intendet to be used seen by everyone - meaning every creature on this planet or the next.

- [conference notes](https://gitlab.com/ai4ce/public-info/-/tree/main/conferences/notes)
