# Interaces between AI4CE modules

## User -> System Generator
project toml, as it can be found [here](https://gitlab.com/ai4ce/public-info/-/blob/main/docs/project_config_opssat.toml?ref_type=heads)

## System Generator -> eFuncs

SysGen Output = eFuncs Input

```python
io = {
    comp_1_uid : {
        comp_paramter1 : ""
        comp_paramter2 : float
    },
    comp_2_uid : {
        comp_paramter1 : ""
        comp_paramter2 : float
    }
} 
```
## eFuncs --> System Generator

eFuncs Output = SysGen Input

```python
io = {
    reward_system : float,
    subsystem1 : {
        reward_component1 : float,
        reward_component2 : float,
        reward_subsystem1: float,
    }, 
    subsystem2 : {
        reward_component1 : float,
        reward_component2 : float,
        reward_subsystem2: float,
    }, 
}
```


## System Generator --> System Analysis tools