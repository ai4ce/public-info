from collections import OrderedDict
import toml

import streamlit as st
import httpx

st.set_page_config(
    layout="wide"
)


def fetch_toml_from_gitlab(url: str) -> dict:
    """
    Fetch a TOML file from a public GitLab repository and return its content as a dictionary.

    Args:
        url (str): The URL of the TOML file in the GitLab repository.

    Returns:
        dict: The content of the TOML file as a dictionary.
    """
    response = httpx.get(url)
    # Raise an exception if the GET request is unsuccessful.
    response.raise_for_status()

    if response.raise_for_status() == "101":
        toml_content = toml.load("docs/parameter_unit_list.toml")
    else:
        toml_content = toml.loads(response.text)
    return toml_content


def update_session_state():
    st.session_state["new_entry"][parameter_name]["unit_name"] = unit_name
    st.session_state["new_entry"][parameter_name]["unit_symbol"] = unit_symbol
    st.session_state["new_entry"][parameter_name]["parameter_type"] = parameter_type
    st.session_state["new_entry"][parameter_name]["parameter_type_text_options"] = st.session_state["parameter_type_text_options_list"]
    st.session_state["new_entry"][parameter_name]["parameter_description"] = parameter_description
    st.session_state["new_entry"][parameter_name]["comment"] = comment


new_entry: dict = {}

st.title("Parameter Creation UI")

# list_gitlab: dict = fetch_toml_from_gitlab(
#     url="https://gitlab.com/ai4ce/public-info/-/raw/main/docs/parameter_unit_list.toml?ref_type=heads")
list_gitlab: dict = toml.load("docs/parameter_unit_list.toml")


parameter_type_options: list = list_gitlab["__param_extras__"]["parameter_type_options"]

col1, col2 = st.columns([1, 1])

if "new_entry" not in st.session_state:
    st.session_state["new_entry"] = {}
if "parameter_type_text_options_list" not in st.session_state:
    st.session_state["parameter_type_text_options_list"] = []

with col1:

    parameter_name: str = st.text_input("Parameter Name")

    if st.button("Create Parameter"):
        st.session_state["new_entry"][parameter_name] = {}

    unit_name: str = st.text_input(label="Unite Name")
    unit_symbol: str = st.text_input(label="Unite Symbol")
    parameter_type: str = st.selectbox(
        label="Parameter Type", options=parameter_type_options)

    if parameter_type == "text":
        parameter_type_text_option: str = st.text_input("Parameter Option")
        if st.button("Add Option"):
            st.session_state["parameter_type_text_options_list"].append(
                parameter_type_text_option)

    parameter_description: str = st.text_input(label="Parameter Description")
    comment: str = st.text_input(label="Comment")

    if st.button("Create/Update", on_click=update_session_state(), key="create"):
        update_session_state()


with col2:
    # st.dataframe(st.session_state["new_entry"]["asdf"], )
    st.write(st.session_state["new_entry"])
    st.divider()
    st.write(st.session_state["parameter_type_text_options_list"])

list_gitlab_added: dict = {}
# list_gitlab[parameter_name] = st.session_state["new_entry"][parameter_name] = {}


def add_to_dict(original_dict: dict, new_dict: dict) -> dict:
    """
    Combine two dictionaries into one.

    Args:
        original_dict (dict): The original dictionary.
        new_dict (dict): The new dictionary to be added.

    Returns:
        dict: The combined dictionary.
    """
    added_dict: dict = original_dict | new_dict
    return added_dict


def sort_dict_alphabetically(updated_dict: dict) -> dict:
    """
    Sort a dictionary alphabetically by its keys.

    Args:
        updated_dict (dict): The dictionary to be sorted.

    Returns:
        dict: The sorted dictionary.
    """
    sorted_dict = OrderedDict(sorted(updated_dict.items()))
    return sorted_dict


if st.button("Combine dicts"):
    st.session_state["list_gitlab_added"] = add_to_dict(
        list_gitlab, st.session_state["new_entry"])
    st.write(st.session_state["list_gitlab_added"])

if st.button("Sort dicts"):
    st.session_state["list_gitlab_added_sorted"] = sort_dict_alphabetically(
        st.session_state["list_gitlab_added"])
    st.write(st.session_state["list_gitlab_added_sorted"])


col21, col22 = st.columns([1, 1])
with col22.expander("Added List in GitLab", expanded=False):
    st.write(st.session_state["list_gitlab_added_sorted"])

with col21.expander("Whole List in GitLab", expanded=False):
    st.write(st.session_state["list_gitlab_added_sorted"])

if st.button("Export to t.toml"):
    with open("t.toml", 'w') as file:
        toml.dump(st.session_state["list_gitlab_added_sorted"], file)
