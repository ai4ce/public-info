# Systems with varrying system complexity

- Test0 - Battery, Solar Panel, Reaction wheel
- Test1 - Battery, Solar Panel, Frame
- Test2 - Battery, Solar Panel, Frame, Camera
- Test3 - Battery, Solar Panel, Frame, Camera, Reaction Wheel
- Test4 - Battery, Solar Panel, Frame, Camera, Reaction Wheel, OBC
- Test5 - Battery, Solar Panel, Frame, Camera, Reaction Wheel, OBC, Transceiver



# OPS-Sat

## Component List
- Battery: GomSpace BP-X battery pack with 8 cells
- Camera
- ADCS
    - coarse ADCS: Implemented on OBC utilizing magnetorquers
        - accuracy of about 12°
        - actuators
            - GomSpace body panels can include air core magnetorquers
    - fine ADCS: iADCS-100 by BST (Berlin Space Technologies) and Hyperion Technologies B.V. of Delft
        - poiting accuracy: beter then 1°
        - sensors:
            - ST-200 star tracker
            - 3-axis Honeywell MC5843 magnetometer on the NanoMind
            - sun sensors located on the exterior solar panels
            - 3-axis gyro can be connected to the NanoMind
- OBC: GomSpace NanoMind A712D
- Solar Panels
- Transceiver
    - X-Band: EWC31 S-band TT&C transceiver from Syrlinks
        - Data-rate: up to 100 Mbit/s
        - Power: 10W
        - Size: 0.25U
    - S-Band: GomSpace ???
        - Data-rate: 1Mbit/s down and 256kbit/s up
    - UHF transceiver: GomSpace NanoCOM AX100
        - frequency: UHF – 437.2 MHz
- Structure
    - Size: 0.1m by 0.1m by 0.3m
- Total
    - Size: 0.1m by 0.1m by 0.3m
    - Mass: 5.4kg

## Requirements
- Must allow easy and complete replacement of on-board software in flight - i.e. an open experimental platform
- Software should be able to be updated quickly, easily and often – a complete reload of the entire software in less than 3 passes
- Allow the use of standard terrestrial CPU, OS and Java – aim for the equivalent of mobile phone performances
- The satellite should be no bigger than a 3U CubeSat (30 x 10 x 10 cm) and compatible for launch in a P-POD
- COTS units shall be used wherever possible (no new developments)
- Cost to be kept below 2 MEuro
- Time to launch between 1-2 years from kick-off
- Single string implementation but reduce single-point failures to a minimum
- Make the satellite safe by design (even without the processor running)
- Do not assume that any unit will work all the time
- Always ensure the ground can recover any unit.

### In the MRD (Mission Requirements Document) the mission statement is formulated as follows (Ref. 5):
- OPS-SAT shall provide an in orbit platform that through reconfigurability shall allow experimenters to uplink and execute software experiments that will advance the state-of-the-art of mission operations and have a clear potential for use in future ESA missions.
- Experimenters are entities within ESA and within the European academic and industrial community that can deliver relevant software experiments compliant with the OPS-SAT capabilities. Software experiments shall have open access to all onboard resources and systems unless justified due to safety.
- The mission shall be realized as a nanosatellite mission levering CubeSat COTS components where possible, without compromising a minimum lifetime target of two years.
- The spacecraft shall be powered and thermally safe even if tumbling. The mission shall be robust against single-event upsets, latching events or faulty experimental software. It shall be demonstrated that despite using COTS components a reconfigurable and yet reliable platform can be delivered.
- The OPS-SAT payload shall deliver as a minimum; two processors running at 500+MHz with 500 MB of RAM, 10 GB solid storage and a reconfigurable FPGA.
- At least one configuration shall be representative of an ESA mission (including ground software and OBSW (On-Board Software).
- S-band uplink rates of at least 256 kbit/s and S-band downlink rates of 1 Mbit/s shall be supported. The high uplink data rate is due to the fact that this mission is focused on new software experiments. Therefore it is needed to upload the frequently changing software experiments at a reasonable time.
- The spacecraft shall be recoverable and resettable by at least two independent communications routes in hardware and software. The spacecraft shall be able to communicate with the respective ground station in any orientation

## Resources
- [Good System Overview incl Components](https://opssat1.esoc.esa.int/attachments/download/922/ESOC%20LunchtimeLLecture2024-01-24.pdf)


# Ideas for other Use Cases
- UPSat
