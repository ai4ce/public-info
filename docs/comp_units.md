# Suggestions for Components Specification Units

Shared Parameters:
| Parameter | Unit | Comment |
| --- | --- | --- |
| source | | string |
| uid | | int |
| name | | string |
| product_page | | string |
| mass | kg | |
| data_interface | | string |
| width | m | |
| length | m | |
| height | m | |
| tags | | string list |
| id | | int |
| volume | m^3 | could be added as a commen parameter|



Heater:
| Parameter | Unit | Comment |
| --- | --- | --- |
| resistancy_per_sqm | Ω/m^2 | |
| lifecycles |  | |
| operating_temperature | °C | switching to K (list) |

MultilayerInsulation:
| Parameter | Unit | Comment |
| --- | --- | --- |
| epsilon | | not shure is a dict |
| k | | not shure is a dict |
| lifecycles |  | |
| operating_temperature | °C | switching to K (list) |

HeatSwitch:
| Parameter | Unit | Comment |
| --- | --- | --- |
| control_temperature | K | switching to K (list) |
| max_power | W | |
| stored_energy | J | Often given in Wh |
| lifecycles | | |
| operating_temperature | °C | switching to K (list) |

Radiator:
| Parameter | Unit | Comment |
| --- | --- | --- |
| lifecycles | | |
| operating_temperature | °C | switching to K (list) |
| total_efficiency | % | |
| thermal_conductivity | W/(m·K) | |
| heat_rejection_cap_per_sqm | J/m^2 | |
| areal_density | kg/m^2 | not in db 1.7.7 addition from db.json |
| hinge_conductance | W/K | not in db 1.7.7 addition from db.json |
| deployment_angles | deg | not in db 1.7.7 addition from db.json |

Louver:
| Parameter | Unit | Comment |
| --- | --- | --- |
| lifecycles |  | |
| operating_temperature | °C | switching to K (list) |
| setpoint | | |
| emissivity_eff | % |  |

Coating:
| Parameter | Unit | Comment |
| --- | --- | --- |
| lifecycles |  | |
| alpha_BOL | | |
| alpha_EOL | | |
| epsilon_BOL | | |
| epsilon_EOL | | |
| coating_perc | | |

Control Moment Gyroscope:
| Parameter | Unit | Comment |
| --- | --- | --- |
| angular_momentum_storage | N·m·s | |
| torque | N·m | |
| power_consumption | W | |
| input_voltage | V | |
| export_control | | string |
| lead_time | s | |

Magnetorquer:
| Parameter | Unit | Comment |
| --- | --- | --- |
| diameter | m | |
| magnetic_moment | A·m^2 | |
| operating_temperature | K | |
| mechanical_vibration | Grms | Root-Mean-Square Acceleration: https://femci.gsfc.nasa.gov/random/randomgrms.html |
| power_consumption | W | |
| input_voltage | V | |
| linearity | % | |
| residual_moment | Am^2 | unsure taken from db.json |
| max_continuous_current | A | |
| magnetic_gain | Am^2/A | unsure taken from db.json |
| electrical_resistance | Ω | |
| connector_type | | string |
| torque | N·m | |
| storage_temperature | K | |
| power_consumption_per_magnetic_moment | W/Am^2 | |
| mass_per_length | kg/m | |

Reaction Wheel:
| Parameter | Unit | Comment |
| --- | --- | --- |
| diameter | m | |
| maximum_torque | N·m | |
| angular_momentum_storage | N·m·s | |
| operating_temperature | K | |
| power_consumption | W | |
| angular_speed | rad/s | often given in rpm |
| input_voltage | V | |
| radiation_tolerance | Gy | Gray instead of Rad |
| mechanical_vibration | Grms | Root-Mean-Square Acceleration: https://femci.gsfc.nasa.gov/random/randomgrms.html |
| static_imbalance | g·m | |
| dynamic_imbalance | g·m | |
| lifetime | s | |
| connector_type | | string |
| speed_control_accuracy | rad/s | often given in rpm |
| rotor_moment_of_inertia | kg·m^2 | |
| lead_time | s | |

Integrated ADCS:
| Parameter | Unit | Comment |
| --- | --- | --- |
| input_voltage | V | |
| pointing_accuracy | deg | |
| lifetime | s | |
| operating_temperature | K | |
| power_consumption | W | |
| angular_momentum_storage | N·m·s | |
| slew_rate | deg/s | |
| maximum_torque | N·m | |
| storage_temperature | K | |
| magnetic_dipole_moment | A·m^2 | |
| lead_time | s | |

Integrated System:
| Parameter | Unit | Comment |
| --- | --- | --- |
| diameter | m | |
| input_voltage | V | |
| operating_temperature | K | |
| lifetime | s | |
| power_consumption | W | |

Accelerometer:
| Parameter | Unit | Comment |
| --- | --- | --- |
| diameter | m | |
| operating_temperature | K | |
| mechanical_shock | g | |
| input_voltage | V | |
| input_current_quiescent | A | |
| update_rate | Hz | |
| update_rate_max | Hz | |
| current | A | |

Attitude Camera:
| Parameter | Unit | Comment |
| --- | --- | --- |
| diameter | m | |
| input_voltage | V | |
| power_consumption | W | |
| onboard_data_storage | B | in Bytes |
| operating_temperature | K | |
| lens_space_qualified_ruggedized | | string |
| exposure_time_range | s | |

Earth Horizon Sensor
| Parameter | Unit | Comment |
| --- | --- | --- |
| input_voltage | V | |
| power_consumption | W | |
| operating_temperature | K | |
| field_of_view_accuracy | deg | |
| radiation_tolerance | Gy | Gray instead of Rad |
| mechanical_vibration | Grms | Root-Mean-Square Acceleration: https://femci.gsfc.nasa.gov/random/randomgrms.html |
| update_rate | Hz | |
| lifetime | s | |
| field_of_view_vertical | deg | |
| field_of_view_horizontal | deg | |
| field_of_view | deg | |
| storage_temperature | K | |
| slew_tolerance | deg/s | |
| pointing_mode_linear_range_roll | deg | |
| pointing_mode_linear_range_pitch | deg | |
| operating_band | m | wavelength |
| lead_time | s | |
| consumption | W | |
| chord_mode_linear_range_roll | deg | |
| chord_mode_linear_range_pitch | deg | |
| acquisition_mode_sign_range_roll | deg | |
| acquisition_mode_sign_range_pitch | deg | |
| acquisition_mode_linear_range_roll | deg | |
| acquisition_mode_linear_range_pitch | deg | |
| accuracy__sigma_eol | deg | guess did not find in db.json |

Gyroscope:
| Parameter | Unit | Comment |
| --- | --- | --- |
| diameter | m | |
| power_consumption | W | |
| input_voltage | V | |
| storage_temperature | K | |
| operating_temperature | K | |
| baud_rate | B/s | bytes per second |
| update_rate | Hz | |
| maximum_torque | N·m | |
| lifetime | s | |
| angular_momentum_storage | N·m·s | |
| mechanical_vibration | Grms | Root-Mean-Square Acceleration: https://femci.gsfc.nasa.gov/random/randomgrms.html |
| sky_coverage | % | |
| scale_factor_stability | 10^-6 | was given in ppm |
| rate_estimation_accuracy_sigma | deg | guess did not find in db.json |
| maximum_slew_rate | deg/s | |
| magnetic_sensitivity | °/hr/gauss  | |
| gyro_dynamic_range | deg/s | |
| detection_capability | Mv | only example of this "Mv ≥5.0" unkown Unit |
| connector_type | | string |
| bias_stability | deg/s | |
| angular_random_walk | deg/hr^0.5 | taken out of the db.json |

IMU:
| Parameter | Unit | Comment |
| --- | --- | --- |
| storage_temperature | K | |
| power_consumption | W | |
| operating_temperature | K | |
| input_voltage | V | |
| baud_rate | B/s | bytes per second |
| update_rate | Hz | |

Magnetometer:
| Parameter | Unit | Comment |
| --- | --- | --- |
| input_voltage | V | |
| operating_temperature | K | |
| power_consumption | W | |
| mechanical_vibration | Grms | Root-Mean-Square Acceleration: https://femci.gsfc.nasa.gov/random/randomgrms.html |
| update_rate | Hz | |
| radiation_tolerance | Gy | Gray instead of Rad |
| magnetic_field_range | kg⋅s^−2⋅A^−1 | given as nT which I guessed as Tesla |
| linearity | % | |
| current | A | |
| magnetic_resolution | | did not find in db.json |
| power_interface | | string |
| orthogonality | deg | |
| magnetic_measurement_range | V | |
| magnetic_field_noise_density | | did not find in db.json |
| connector_type | | string |

Star Tracker:
| Parameter | Unit | Comment |
| --- | --- | --- |
| diameter | m | |
| input_voltage | V | |
| operating_temperature | K | |
| update_rate | Hz | |
| power_consumption | W | |
| pointing_accuracy | deg | |
| lifetime | s | |
| sun_exclusion_angle | deg | |
| storage_temperature | K | |
| field_of_view | deg | |
| survival_temperature | K | |
| slew_rate | deg/s | |
| mechanical_vibration | Grms | Root-Mean-Square Acceleration: https://femci.gsfc.nasa.gov/random/randomgrms.html |
| maximum_tracking_rate | deg/s | |
| field_of_view_vertical | deg | |
| field_of_view_horizontal | deg | |
| current | A | |
| availability | % | |
| attitude_knowledge_error | arcsec | |
| star_sensitivity | mv | did not find Unit taken from db.json |
| star_catalog_size | | amount of stars |
| focal_length | m | |
| aperture | m | |

Sun Sensor:
| Parameter | Unit | Comment |
| --- | --- | --- |
| diameter | m | |
| field_of_view | deg | |
| field_of_view_accuracy | deg | |
| radiation_tolerance | Gy | Gray instead of Rad |
| operating_temperature | K | |
| input_voltage | V | |
| power_consumption | W | |
| shock | g | typically "g" sometimes "g @ 1-100 ms" |
| random_vibration | Grms | given example "18.3 g @ 20-2000 Hz" |
| accuracy | deg | |
| lifetime | s | |
| update_rate | Hz | |
| survival_temperature | K | |
| lead_time | s | |
| mechanical_vibration | Grms | Root-Mean-Square Acceleration: https://femci.gsfc.nasa.gov/random/randomgrms.html |
| connector_type | | string |
| current | A | |
| transition_accuracy | deg | |
| peak_output_current | A | |
| output_voltage | V | |
| mounting_flange | m | |
| body | m | |

Bus:
| Parameter | Unit | Comment |
| --- | --- | --- |
| lifetime | s | |
| payload_mass | kg | |
| operating_temperature | K | |
| payload_power | W | |
| payload_volume | m^3 | |
| data_rate | B/s | bytes per second |
| data_storage | B | in bytes |
| lead_time | s | |
| power | W | |
| maximum_thrust | N·m | |
| pointing_accuracy | deg | |
| pointing_knowledge | deg | |
| power_supply | V | |
| power_supply_eol_oa | W | |
| wet_mass | kg | |
| delivery_time | s | |

DataAcquisitionHandling:
| Parameter | Unit | Comment |
| --- | --- | --- |
| data_storage | B | in bytes |
| operating_temperature | K | |
| power_consumption | W | |
| connector_type | | string |
| voltage | V | |
| average_power_consumption | W | |
| battery_capacity | J | often given in Ah or Wh |
| clock_speed | Hz | |
| current | A | |
| data_downlink | B/s | bytes per second |
| input_voltage | V | |
| network_protocol | | string |
| processor_type | | string |
| radiation_tolerance | Gy | Gray instead of Rad |
| shock | g | typically "g" sometimes "g @ 1-100 ms" |
| supply_voltage | V | |
| vibration | Grms | Root-Mean-Square Acceleration: https://femci.gsfc.nasa.gov/random/randomgrms.html |
| voltage_supply | V | |
| storage_temperature | K | |


OBC:
| Parameter | Unit | Comment |
| --- | --- | --- |
| RAM_size | B | in bytes |
| RAM_ECC_support | | Boolean |
| RAM_DDR_version | | string |
| data_storage_size | B | in bytes |
| data_storage_type | | string |
| operating_temperature | K | |
| power_consumption | W | |
| flash_memory | B | in bytes |
| input_voltage | V | |
| processor_type | | string |
| processor_clock | Hz | |
| radiation_tolerance | Gy | Gray instead of Rad |
| ROM | B | in bytes |
| SDRAM | B | in bytes |
| comp_throughput | FLOPS | |
| lifetime | s | |
| max_clock_speed | Hz | |
| nominal_power_consumption | W | |
| processor | | string |
| SRAM | B | in bytes |
| EEPROM | B | in bytes |
| program_memory | B | in bytes |
| storage_temperature | K | |

Amplifier:
| Parameter | Unit | Comment |
| --- | --- | --- |
| frequency | Hz | |
| output_power | W | |
| power_added_efficency | % | guess did not find in db.json |

Antenna:
| Parameter | Unit | Comment |
| --- | --- | --- |
| diameter | m | |
| antenna_gain | dB | often dBi (antenna is compared to an isotropic radiator) |
| frequency | Hz | |
| operating_temperature | K | |
| polarization | | string |
| RF_output_power | W | |
| axial_ratio | dB | |
| bandwidth | Hz | |
| export_control | | string |
| realized_gain | dB | often dBi (antenna is compared to an isotropic radiator) |
| rf_power | W | |
| input_voltage | V | |
| FB_ratio | dB | |
| VSWR | | Without Units |
| antenna_directivity | dB | often dBi (antenna is compared to an isotropic radiator) |
| connector_type | | string |
| impedance | Ω | |
| ERP | dB | |
| SWR | | Without Units |
| active_gain | dB | dBiC used (decibels referenced to a circularly polarized, theoretical isotropic radiator)|
| antenna_gain_ceramic_patch_only | dBi | |
| beamwidth | dev | |
| current | A | |
| height_without_connector | m | |
| length_without_connector | m | |
| width_without_connector | m | |
| mechanical_vibration | Grms | Root-Mean-Square Acceleration: https://femci.gsfc.nasa.gov/random/randomgrms.html |
| noise_figure | dB | |
| output_VSWR | | Without Units |
| power_consumption | W | |
| radiation_tolerance | Gy | Gray instead of Rad |

Modulator:
| Parameter | Unit | Comment |
| --- | --- | --- |
| input_voltage | V | |
| carrier_frequency | Hz | |
| lifetime | s | |
| output_symbol_rate | Bd | Baud (symbols per second) |
| power_consumption | W | |

SDR:
| Parameter | Unit | Comment |
| --- | --- | --- |
| operating_temperature | K | |
| input_voltage | V | |
| RAM_size | B | in bytes |
| RAM_ECC_support | | Boolean |
| RAM_DDR_version | | string |
| NAND_flash | B | in bytes |
| receive_frequency | Hz | |
| receive_power | W | |
| transmit_power | W | once given dBm in the db.json |

Transceiver:
| Parameter | Unit | Comment |
| --- | --- | --- |
| data_rate | B/s | or in given unit |
| data_rate_unit | | string |
| lifetime | s | |
| operating_temperature | K | |
| power_consumption | W | |
| transmit_frequency | Hz | |
| receive_frequency | Hz | |
| sensitivity | dBm | |
| transmit_power | W | once given dBm in the db.json |
| receive_data_rate | B/s | or bit/s |
| transmit_data_rate | B/s | or bit/s |
| transmit_modulation | | string |
| receive_modulation | | string |
| RF_sensitivity | dBm | |
| channel_data_rate | B/s | |
| channel_data_rate_unit | | string |
| communication_channel | | string |
| connector_type | | string |
| data_storage | B | in bytes |
| datalink_layer_protocol | | string |
| downlink_frequency | Hz | |
| frequency | Hz | |
| input_voltage | V | |
| noise_figure | dB | |
| rf_power | W | often given in dBm |
| height_CPA_terminal | m | |
| length_CPA_terminal | m | |
| width_CPA_terminal | m | |
| uplink_frequency | Hz | |

Transmitter:
| Parameter | Unit | Comment |
| --- | --- | --- |
| data_rate | B/s | or in given unit |
| data_rate_unit | | string |
| frequency | Hz | |
| lifetime | s | |
| operating_temperature | K | |
| modulation | | string |
| baud_rate | b/s | or in given unit |
| baud_rate_unit | | string |
| input_voltage | V | |
| power_consumption | W | |
| transmit_power | W | often given in dBm |
| RF_output_power | W | often given in dBm |
| connector_type | | string |
| internal_memory | B | |
| rf_power | W | often given in dBm |
| snr | dB | |
| transmit_data_rate | B/s | |
| transmit_frequency | Hz | |
| DC_power_consumption | W | |
| bandwidth | Hz | |
| current | A | |
| mass_without_panel_without_DC_harness | kg | |
| mechanical_vibration | Grms | Root-Mean-Square Acceleration: https://femci.gsfc.nasa.gov/random/randomgrms.html |
| radio_signal_frequency_range | Hz | |
| receive_frequency | Hz | |
| rf_output_impedance | Ω | |
| rf_output_power | W | often given in dBm |
| sensitivity | dBm | |
| storage_temperature | K | |
| voltage_standing_wave_ratio | | Without Unit |

EEE:
| Parameter | Unit | Comment |
| --- | --- | --- |
| diameter | m | |
| box_volume | m^3 | |
| operating_temperature | K | |
| current | A | |
| voltage | V | |
| rf_input_power | W | often given in dBm |
| storage_temperature | K | |
| drain_voltage | V | |

Material: (does not include all standart parameters: sizes, mass and data_interface)
| Parameter | Unit | Comment |
| --- | --- | --- |
| operating_temperature | K | |
| storage_temperature_indoors | K | |
| shelf_life | s | |

Mechanism:
| Parameter | Unit | Comment |
| --- | --- | --- |
| diameter | m | |
| operating_temperature | K | |

Mechanism:
| Parameter | Unit | Comment |
| --- | --- | --- |
| diameter | m | |
| operating_temperature | K | |

Structure:
| Parameter | Unit | Comment |
| --- | --- | --- |
| diameter | m | |
| power_consumption | W | |

DeOrbitingDevice:
| Parameter | Unit | Comment |
| --- | --- | --- |

Engine:
| Parameter | Unit | Comment |
| --- | --- | --- |

GNSSGPSReceiver:
| Parameter | Unit | Comment |
| --- | --- | --- |
| diameter | m | |
| GPS_position_accuracy | m | |
| input_voltage | V | |
| operating_temperature | K | |
| GPS_velocity_accuracy | m/s | |
| power_consumption | W | |
| GPS_signal | | string |
| GPS_altitude_accuracy | m | |
| GPS_channels | | Without Unit |
| GPS_time_to_first_fix | s | |
| connector_type | | string |
| mechanical_vibration | Grms | Root-Mean-Square Acceleration: https://femci.gsfc.nasa.gov/random/randomgrms.html |
| storage_temperature | K | |
| frequency | Hz | |
| mechanical_shock | g | |
| operating_frequency | Hz | |
| position_accuracy_1sigma | m | |
| radiation_tolerance | Gy | Gray instead of Rad |
| update_rate | Hz | |
| velocity_accuracy_1sigma | m/s | |

PressureRegulator: (does not include all standart parameters: sizes)
| Parameter | Unit | Comment |
| --- | --- | --- |
| flow_range_GHe | kg/s | |
| inlet_pressure_range | Pa | |
| operating_temperature | K | |
| lifetime | s | |
| outlet_pressure_range | Pa | |
| overall_random_vibration | Grms | Root-Mean-Square Acceleration: https://femci.gsfc.nasa.gov/random/randomgrms.html |
| crack_pressure | Pa | |
| maximum_flow_GHe | kg/s | |
| minimum_reseat_pressure | Pa | |

PropellantTank: (does not include all standart parameters: sizes)
| Parameter | Unit | Comment |
| --- | --- | --- |
| burst_pressure | Pa | |
| operating_temperature | K | |
| residual_volume | m^3 | |
| volume | m^3 | |
| flow_rate | m^3/s | |
| proof_pressure | Pa | |
| tank_pressure | Pa | |
| external_leakage | m^3/s | given as Scc/s (stadart cubic meter) / no SI-Unit fits |

Thruster:
| Parameter | Unit | Comment |
| --- | --- | --- |
| diameter | m | |
| input_voltage | V | |
| operating_temperature | K | |
| specific_impulse | s | |
| total_impulse | N·s | |
| minimum_impulse_bit | N·s | |
| nominal_thrust | N | |
| power_consumption | W | |
| propellant_mass | kg | |
| thrust_range | N | |
| accumulated_firing_time | s | or as given in string |
| accumulated_firing_time_unit | | string |
| firing_sequences | | Unit given in string |
| firing_sequences_unit | | string |
| longest_continious_firing | | Unit given in string |
| longest_continious_firing_unit | | string |
| propellant_throughput | | Unit given in string |
| propellant_throughput_unit | | string |
| pulses | | Unit given in string |
| pulses_unit | | string |
| density_impulse | N·s·m^-3 | |
| dynamic_thrust_range | N | |
| inlet_pressure_range | Pa | |
| regulated_reactor_pre_heating_power | W | |
| steady_state_isp | N·s·kg^-1 | |
| thrust_to_power_ratio | N/W | |
| volume | m^3 | |
| connector_type | | string |
| mechanical_vibration | Grms | Root-Mean-Square Acceleration: https://femci.gsfc.nasa.gov/random/randomgrms.html |
| nominal_reactor_pre_heating_voltage | V | |
| nozzle_expansion_ratio | | Without Unit |
| propellant_type | | string |
| storage_temperature | K | |
| hot_standby_power | W | |

Payload:
| Parameter | Unit | Comment |
| --- | --- | --- |
| lifetime | s | |
| operating_temperature | K | |
| power_consumption | W | |
| input_voltage | V | |
| data_storage | B | in Byte |
| frequency | Hz | |
| lead_time | s | |
| mechanical_vibration | Grms | Root-Mean-Square Acceleration: https://femci.gsfc.nasa.gov/random/randomgrms.html |
| radiation_tolerance | Gy | Gray instead of Rad |

Camera:
| Parameter | Unit | Comment |
| --- | --- | --- |
| diameter | m | |
| operating_temperature | K | |
| ground_sample_distance | m | |
| power_consumption | W | |
| data_storage | B | in Byte |
| lifetime | s | |
| input_voltage | V | |
| radiation_tolerance | Gy | Gray instead of Rad |
| swath | m | |
| aperture | m | |
| control_interface | | string |
| current | A | |
| data_encoding | | string |
| dynamic_range | dB | |
| exposure_time | s | |
| focal_length | m | |
| frame_rate | Hz | fps |
| image_sensor_type | | string |
| number_of_spectral_bands | | Without Unit |
| spectral_band | m | |
| spectral_response | m | |
| storage_temperature | K | |
| supply_voltage | V | |
| survival_temperature | K | |
| channels | | Without Unit |
| data_format | | string |
| distortion | % | |
| field_of_view | deg | |
| focal_ratio | | Without Unit |
| interface | | string |
| lead_time | s | |
| lens_space_qualified_ruggedized | | string |
| mtf_at_lb | | unspecified in db.json |
| pixel_size | m | |

Battery:
| Parameter | Unit | Comment |
| --- | --- | --- |
| battery_type | | string |
| battery_capacity | J | |
| battery_pack_voltage | V | |
| operating_temperature | K | |
| specific_energy | J/kg | |
| storage_temperature | K | |
| total_ionizing_dose | Gy | Gray instead of Rad |
| recommended_charge_rate | A | |
| maximum_discharge_rate | A | |
| heater_power | W | |
| export_control | | string |
| radiation_tolerance | Gy | Gray instead of Rad |
| lower_voltage_limit_for_discharge | V | |
| energy_density | J·m^-3 | |
| discharge_current | A | |
| designed_lifetime | s | |
| charging_voltage_max | V | |
| charging_method | | string |
| charge_current | A | |
| cathode | | string |
| anode | | string |
| pulse_current | A | |
| nameplate_capacity | C | given in Ah --> coulombs |

iEPS:
| Parameter | Unit | Comment |
| --- | --- | --- |
| operating_temperature | K | |
| battery_capacity | J | |
| output_voltage | V | |
| battery_type | | string |
| typical_quiescent_power_consumption | W | |
| total_ionizing_dose | Gy | Gray instead of Rad |
| input_power | W | |
| discharge_current | A | |
| charge_current | A | |
| storage_temperature | T | |
| nominal_power | W | |
| transmit_power | | string |
| transmit_frequency | Hz | |
| receive_frequency | Hz | |
| total_discharge_cycles | | Without Unit |
| system_storage | B | in Byte |
| sensitivity | dBm | |
| radiation_tolerance | Gy | Gray instead of Rad |
| peak_power | W | |
| maximum_power | W | |
| lead_time | s | |
| eirp | dBm | |
| data_rate | B/s | |
| bus_type | | string |

PowerConverter:
| Parameter | Unit | Comment |
| --- | --- | --- |
| operating_temperature | K | |
| total_efficiency | % | |
| input_voltage | V | |
| input_power | W | |
| output_voltage | V | |
| storage_temperature | K | |
| radiation_tolerance | Gy | Gray instead of Rad |

PowerDistributionControl:
| Parameter | Unit | Comment |
| --- | --- | --- |
| operating_temperature | K | |
| input_voltage | V | |
| battery_protection | | Boolean |
| num_of_channels | | Without Unit |
| survival_temperature | K | |
| radiation_tolerance | Gy | Gray instead of Rad |
| power_output | W | |
| output_voltage | V | |
| load_current | A | |
| lifetime | s | |
| input_current | A | |
| connector_type | | string |
| battery_capacity | J | |

SolarPanel:
| Parameter | Unit | Comment |
| --- | --- | --- |
| operating_temperature | K | |
| solar_power | W | |
| peak_current | A | |
| output_voltage | V | |
| lifetime | s | |
| power_efficiency | % | % at EOL |
| wing_sizes | W | given in Watt but Area would make more sence |
| watts_per_square_meter | W/m^2 | |
| watts_per_kg | W/kg | |
| volume | m^3 | |
| kilowatts_per_cubic_meter | W/m^3 | |
| end_of_life_power | W | |

Source:
| Parameter | Unit | Comment |
| --- | --- | --- |
| type | | string |
| peak_power | W | |
| source_type | | string |
| operating_temperature | K | |
| peak_current | A | |
| voltage | V | |
| lifetime | s | |
| power_efficiency | % | % at EOL |
| diameter | m | |
| diameter | m | |