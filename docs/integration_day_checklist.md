
# Goal
- test every interface/connection individually
- test full chain of implemented blocks
  - define system -> generate design -> show result
- 1 docker compose file to launch everything
  - pull from every repo
  - build the docker container for each
    - each repo has a similar docker build file

# Desired behaviour of the AI4CE software
``` bash
$ ai4ce start
$ >>> Welcome to the AI4CE tool v0.1. We now start the full feature of services:
$ >>> Starting: backend ...
$ >>> Starting: sys arch builder ...
$ >>> Starting: g3Del ...
$ >>> Starting: web GUI ...
$ >>> Nice. All services up and ready.
$ >>> go to: localhost:1234
```




# Checklist
- [ ] Backend: launch services
- [ ] Backend: test connections
- [ ] SysArch: define a test system
  - [ ] Thermal only
- [ ] RL: implement RL env with necessary system evaluation function
- [ ] RL: make test selection
- [ ] backend&RL: make component requests
- [ ] g3Del: visualise generated system
