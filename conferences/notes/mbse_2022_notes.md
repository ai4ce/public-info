# MBSE 2022 @ Airbus Leadership Academy, Toulouse, France
2022-11-22..24

📄 check out the paper 
🛝 slides
📷 like at the photo
🎁 follow up for further exchange


## Ralf Hartmann 
[Ralf Hartmann | LinkedIn](https://www.linkedin.com/in/ralf-hartmann-1a03a214/) - [International Council on Systems Engineering Website](https://www.incose.org/) 
- INCOSE
- Global organisation of MBSE supporters
- Future of MBSE
- Fields where AI can be used

## Jean-luc voirin Thales
- future problems and possible solutions 
- Challenge1 mastering big data (engineering assets)
- 100,000+ engineering data -> needs a data revolution 
- Challenge 2 the wall of complexity
- Challenge3 design for trust
- Ai , system of system
- key enabler 1 a true workflow and data driven engineering support
- Key enabler 2 guiding assisting justifying activities and results
- Great slide!
- Key enabler 3 impact analysis and automation service
- Show stopper1 silos in engineering data and tools 
- AI4CE breaks the silos between SE and HW
- Stopper 2 segmantic silos on manually built linked data

## Marco Ferrogalini Airbus

## Joachim Fuchs (ESA)
It is important to see MBSE in it’s ecosystem 

## 📄 Jean-Loup Terraillon (ESA)
- status of MBSE in ESA
- MBSE is a evolution and change process 
- MB4SE AG


## Alexandre Cortier

## 📄 Katarina Jesswein, Michael Brahm OHB
- Problems in translating requirements from natural language 
- requirement Engineering 


## Stéphane Estable Airbus
- beyond SysML
- Tools used in MBSE
- MBSE is a new discipline. 
- It is about defining the system 


## Hazel Jeffrey craft prospect 
"_MBSE forces you to focus on Functional before physical_"

## Marcel Verhoef ESA 


## Carlos redondo gmv 
System factory 


## Julien Baclet irt
- Teepee
- Complete suite of MBSE tools 


## 📄Gianmaria Bullegas -  Perpetual Labs
- GitWorks by Perpetual Labs
- Github for MBSE DevOps 
- With a CI pipeline that build documents every time something changes 

## Armin Müller ScopeSET
[Executive_Summary.pdf]()
- Imap Integrated MBSE Analytics Platform 


# Day2

## Serge Valera ESTEC
LinkedIn
- Working on OsMoSe

## Yuta Nakajima JAXA
- we need flexibility in exchanging models 
- Semantic interoperability 
- Ground Segment and Operations as dedicated topic 
- Semicoma 
- How to work with different versions 
- Modelling with ORM


## Jean-Baptist ESA
- Continuation of last presentation 
- examples of how to model with osmoses ORM
- MBSE main concepts 

## Lucie Laborde - Airbus DS 
LinkedIn
- Continuation of last presentation 
- MBSE main concepts Events 
- Relation to SysML activity diagram 



## Michael Brahm OHB
- 🛝40-- mapping of the metabodel 



## Serge Valera ESA
LinkedIn
- "semantic is often hardcoded into the software "
- "What we want to archive with space system ontology: Semantic put into the model"
- As an distributes system 
- 🛝62 📷"I want to trust no one" -> independent verification facility 


## Lucie Laborde - Airbus DS
- SSO space system ontology 
- Large system with lots of documents 


## Elaheh Maleki ESTEC
LinkedIn
- Modelled in ORM
- 🛝4 Ontology based System Engineering 
- MBSE4Thermal
- How to model thermal systems semantically


## Tobias Hoppe Airbus
—> RHEA’s MBSE Hub activities 
- MBEH Model Based Engineering Hub
- MBSE Data Hub
- Data exchange between mission and stake holders 
- All have different tools and Workflows and analysis methods (dashboards ..)
- Phase1 scoping architectural design
- Phase2 dev and demo
- Interaction with hub via Rest oder interfaces
- Elastic search index 
- Web interface

## 📄 Alex Vorobiev RHEA
- MBEH
- CIP common information platform
- Kalliope model based software engineering 
- Validation of the data towards SSO compliance is implemented into the MBEH

## Nieves RHEA
- MBSEHub AIV
- AIV Assembly Implementation Validation
- Kalliope: ORM —> JSON including validation 

## Maxime Perrotin ESA
- Improve software development 

## Steve Ducan
- MBSE for flight software development 
- TASTE framework for software dev 
- Embedded system development with MBSE method 
- Open source TASTE

## 📄 Iulia Dragomir gmv 
- Requirement validation for TASTE
- Model checking 
- Automated 
- Open source verimag / IF / IF Toolset · GitLab

## Filip Demski N7 space 
TASTE

## 📄 Petros Pissias ESOC
- MBSE for mission OPS
- ORM
- OPS-Sat and ML
- ORM made available 


## 📄 Jamie Whitehouse Aurora Technology
- ESA SysML Solution 
- Addition to SysML
- Enterprise architect 
- Cameo System Modeller
- 🛝3 Link to MBSE2021
- Mappable to target languages


## Hans-Peter de Koning  DEKonsult
- the final submission for the SysML V2
- 🛝2 What is SysML
- Jupyter Notebook 


## 📄 Gérald Garcia (Thales Alenia Space)
- Audrey
- NLP for CE
- AI needs data
- Exago - engineering Plattform 
  - Advanced IT Infrastruktur 
  - CI/CD
  - Kubernetes
  - Cyber sec 


# Day3 

## 📄 Yuta Nakajima JAXA 
- MBSE failure mode analysis
- Web
- SysML & R & markdown
- Open source library 



## ❤️📄 Jamie Whitehouse Aurora/Estec 
- Plato Mission
- Python extensions to comet
- Plato mission parameter database management plan 



## Sam Grene RHEA
- 10-25 A BC
- Updates to comet server 
- New web app


## ❤️ Marcos Eduardo Rojas Ramirez
- European Spaceship
- 🛝6 SysML vs Capella 
- testing MBSE solutions with them


## Jean-Marie Gauthier (IRT Saint Exupéry)
- EasyMOD
- Dashboard 
- TeePee


## 🎁 Rodrigo Ramos
- System Engineer
- Non-functional mission requirements database 
- Requirements easiest defined by use cases
- "Ecss standard would speed run system development"
- SysML by for CubeSat
- João Paulo Monteiro PhD Student in System Engineering 
- Helped Rodrigo

# Poster

📄Bristol landscape 
📄Aachen Deni Raco
Madrid
📄Loriene requirement 
MIT
Toulouse value modelling 
Toulouse system consistency, ops diagnostics

3: Deni Raco RWTH Aachen 
SysMLv2 eigenes Profil
Requirement correctness and verification 

